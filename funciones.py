#Funciones
from string import punctuation
import unicodedata
import re
import nltk
import numpy as np
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
from nltk.stem import SnowballStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from os import path
from PIL import Image
stemmer_spanish = SnowballStemmer('spanish')


def remove_punctuation(string):
    chars = [c for c in string if not c in punctuation]
    return "".join(chars)

def remove_accents(text):
    text = unicodedata.normalize('NFKD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    return re.sub('[^a-zA-Z0-9 \\\]', '', text)

def quitaNoAlfaNum(texto):
    import re
    return re.compile(r'\W+', re.UNICODE).split(texto)

def remove_stopwords(sentence):
    word = ''
    for token in nltk.word_tokenize(sentence):
        if token.lower() not in stopwords.words('spanish'):
            word += ''.join(token)+ ' '
    return(word.strip())

def show_wordcloud(data, title = None):
    """Fuente: https://stackoverflow.com/questions/16645799/how-to-create-a-word-cloud-from-a-corpus-in-python"""
    wordcloud = WordCloud(
        background_color='white',
        stopwords=stopwords,
        max_words=200,
        max_font_size=40, 
        scale=3,
        random_state=1 # chosen at random by flipping a coin; it was heads
    ).generate(str(data))

    fig = plt.figure(1, figsize=(12, 12))
    plt.axis('off')
    if title: 
        fig.suptitle(title, fontsize=20)
        fig.subplots_adjust(top=2.3)

    plt.imshow(wordcloud)
    plt.show()